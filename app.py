import base64
import streamlit as st
from utils.background import set_background
from streamlit_option_menu import option_menu
from streamlit_extras.switch_page_button import switch_page
from streamlit_extras.add_vertical_space import add_vertical_space
from streamlit_extras.row import row


st.set_page_config(page_title="Simply.Find", page_icon="😎", layout="wide")


@st.cache_data
def get_img_as_base64(file):
    with open(file, "rb") as f:
        data = f.read()
    return base64.b64encode(data).decode()


def breaking_line():
    st.markdown("----")


#img = get_img_as_base64("images/background-final.png")

#page_bg_img = set_background(img)

#st.markdown(page_bg_img, unsafe_allow_html=True)


selected = option_menu(None, ["Home", "Products", "Tasks", 'Settings'],
                       icons=['house', 'piggy-bank', "list-task", 'gear'],
                       menu_icon="cast", default_index=0, orientation="horizontal")

st.image("images/background-final.png")

if selected == "Products":
    switch_page("product")

st.title(':orange[Simply.Find]')



st.title(":orange[Deals on TVs] 🚨")
col1, col2, col3 = st.columns(3)

with col1:
    st.subheader("A cool TV")
    st.image("images/products/tv/8.jpg")
    st.success("🥁 :red[Deal]  M70B Series 4K UHD USB-C Smart Monitor & Streaming TV,")

with col2:
    st.subheader("Ultra HD")
    st.image("images/products/tv/11.jpeg")
    st.success("🥁 :red[Deal] M80C UHD HDR Smart Computer Monitor Screen with Streaming TV")

with col3:
    st.subheader("Another cool TV")
    st.image("images/products/tv/12.jpeg")
    st.success("🥁 :red[Deal]       40-inch D-Series Full HD 1080p Smart TV and Cat TV")

breaking_line()
st.title(":orange[Furniture Deals] 🚨")

col3, col4, col5 = st.columns(3)
with col3:
    st.subheader("Sofa 150 x 85")
    st.image("images/products/home/32.jpg")
    st.success("🥁 :red[Deal] Sofa 150 x 85 cm - with Sleep Function - Modern Sofa")

with col4:
    st.subheader("2-Seater Sofa")
    st.image("images/products/home/40.jpg")
    st.success("🥁 :red[Deal] 2-Seater Sofa, Velvet Small Couch with 2 Throw Cushions")

with col5:
    st.subheader("Silver Design")
    st.image("images/products/home/41.jpg")
    st.success("🥁 :red[Deal]       Design Furniture 137 x 76 x 79 silver home collection")

breaking_line()
st.markdown("### :red[Learn more] 🤓")


def links():
    links_row = row(3, vertical_align="center")
    links_row.link_button(
        "📖 Visit our FAQ",
        "ssss",
        use_container_width=True,
    )
    links_row.link_button(
        "🐙 Visit our repository",
        "ssss placeholder",
        use_container_width=True,
    )
    links_row.link_button(
        "🌎 Global operation",
        "ssss",
        use_container_width=True,
    )


links()

st.info(" Simply. Find Store -- 2024  ")
