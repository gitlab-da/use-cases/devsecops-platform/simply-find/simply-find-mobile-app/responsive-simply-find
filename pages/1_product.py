import os
import json
import yaml
import base64
import requests
import streamlit as st
from utils.background import set_background
from utils.product_rows import render_product_row


def load_config(): # this will be for Code suggestions
    with open("config.yaml", "r") as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)


@st.cache_data
def get_img_as_base64(file):
    with open(file, "rb") as f:
        data = f.read()
    return base64.b64encode(data).decode()


def construct_image_paths(image_names, script_directory, product_type):
    return [os.path.join(script_directory, '..', 'images', 'products', product_type, f"{image}.jpg") for image in image_names]


def breaking_line():
    st.markdown("----")


def search(query, endpoint):
    query = {'search_query': query }
    results = requests.post(url=endpoint, json=query)
    #print(results.json())
    if results.status_code == 200:
        results = json.loads(results.json())
        return results
    else:
        return "It is not you, it's us. No results"


config = load_config()
endpoint = config['search']['endpoint']


img = get_img_as_base64("images/mandarinky-background.jpeg")

page_bg_img = set_background(img)

st.page_link("app.py", label="Home", icon="🏠")
st.subheader(":orange[Product List]")

st.markdown(page_bg_img, unsafe_allow_html=True)
st.sidebar.header("Filter Options")

st.subheader("Looking for products  🔍 ? ")
st.markdown("###### Not sure about the product name 🤔? try describing it")


query = st.text_input(label="Search", value="")
search_results = search(query=query, endpoint=endpoint)
if search_results:
    print(type(search_results))
    st.write(search_results[0]['series_title'])
    st.image(search_results[0]['poster_link'])


with st.sidebar:
    # category = st.sidebar.selectbox("Select Category", ["All", "Electronics", "Clothing", "Books"])
    books = st.checkbox('Books')
    computers = st.checkbox('Computers & Accesories')
    healthy_food = st.checkbox('Healthy Food')
    home = st.checkbox('Home & Kitchen')
    movies = st.checkbox('Movies & TV')
    software = st.checkbox('Software')
    st.subheader("Choose shipping method")
    add_radio = st.radio(
        "",
        ("Standard (5-15 days)", "Express (2-5 days)")
    )

image_names_1 = ['indiana_jones', 'up', 'matrix']
image_names_2 = ['godfather', 'whiplash', 'forrest_gump']
movies_headers = ['Indiana Jones', 'UP', 'The Matrix', 'The Godfather', 'Whiplash', 'Forrest Gump']

script_directory = os.path.dirname(os.path.abspath(__file__))
full_image_paths_1 = construct_image_paths(image_names_1, script_directory, 'movies')
full_image_paths_2 = construct_image_paths(image_names_2, script_directory, 'movies')


if movies:
    render_product_row(full_image_paths_1, movies_headers)
    render_product_row(full_image_paths_2, movies_headers[3:])




fruits_images = ['apple', 'banana', 'berry', 'coconut', 'pineapple', 'strawberry']
fruits_headers = ['Apple', 'Banana', 'Berry', 'Coconut', 'Pineapple', 'Strawberry']

fruits_image_paths = construct_image_paths(fruits_images, script_directory,'fruits')

if healthy_food:
    render_product_row(fruits_image_paths, fruits_headers)
    render_product_row(fruits_image_paths[3:], fruits_headers[3:])
    
col_4, col_5, col_6 = st.columns(3)

with col_4:
    st.subheader("Cat movie")
    st.image("https://static.streamlit.io/examples/cat.jpg")

with col_5:
    st.subheader("The Owl")
    st.image("https://static.streamlit.io/examples/owl.jpg")

with col_6:
    st.header("The Dog")
    st.image("https://static.streamlit.io/examples/dog.jpg")
