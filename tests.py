import streamlit as st
import os


def render_product_row(product_image, header):
    col_1, col_2, col_3 = st.columns(3)
    script_directory = os.path.dirname(os.path.abspath(__file__))
    image_path = os.path.join(script_directory, '..', 'images', 'products', 'movies', f"{product_image}.jpg")
    print(image_path)

    with col_1:
        st.subheader(header[0])
        st.image(image_path)
        st.success(f"🥁 :red[Deal]  M70B Series 4K UHD USB-C Smart Monitor & Streaming TV,")

    with col_2:
        st.subheader(header[1])
        # Adjust the image path for the second column
        st.image(image_path)  # Replace with the correct image path
        st.success(f"🥁 :red[Deal] M80C UHD HDR Smart Computer Monitor Screen with Streaming TV")

    with col_3:
        st.subheader(header[2])
        # Adjust the image path for the third column
        st.image(image_path)  # Replace with the correct image path
        st.success(f"🥁 :red[Deal]       40-inch D-Series Full HD 1080p Smart TV")

# Assuming you have three distinct images for each column
image_paths = ['indiana_jones', 'up', 'matrix']
column_headers = ['UHD TV', 'Ultra HD', 'Another cool TV']

for image_path, header in zip(image_paths, column_headers):
    render_product_row(image_path, column_headers)
