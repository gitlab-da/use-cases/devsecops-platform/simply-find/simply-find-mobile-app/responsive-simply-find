import os
import streamlit as st


def render_product_row(product_image, header):
    col_1, col_2, col_3 = st.columns(3)
    script_directory = os.path.dirname(os.path.abspath(__file__))
    image_path = os.path.join(script_directory, '..', 'images', 'products', 'movies', f"{product_image}.jpg")
    print(image_path)

    with col_1:
        st.subheader(header[0])
        st.image(product_image[0])
        st.warning("📽️ :red[Blu-Ray]")

    with col_2:
        st.subheader(header[1])
        st.image(product_image[1])
        st.warning("📽️ :red[Only 2 left in stock]")

    with col_3:
        st.subheader(header[2])
        st.image(product_image[2])
        st.warning("📽️ :red[4K Ultra HD]")