
def set_background(img):

    background_style = f"""
    <style>
        [data-testid="stAppViewContainer"] > .main {{
        background-image: url("data:image/png;base64,{img}");
        background-size: 100%;
        background-position: top left;
        background-repeat: no-repeat;
        background-attachment: local;
    }}

        [data-testid="stSidebar"] > div:first-child {{
        background-image: url("data:image/png;base64,{img}");
        background-position: center; 
        background-repeat: no-repeat;
        background-attachment: fixed;
    }}

        [data-testid="stHeader"] {{
        background: rgba(0,0,0,0);
    }}

        [data-testid="stToolbar"] {{
        right: 2rem;
    }}
    </style>
"""

    return background_style

