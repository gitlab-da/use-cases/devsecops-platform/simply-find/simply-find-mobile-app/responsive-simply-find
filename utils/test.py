import streamlit as st
import os


import os
import streamlit as st

def render_product_row(product_images, headers):
    col_1, col_2, col_3 = st.columns(3)

    with col_1:
        st.subheader(headers[0])
        st.image(product_images[0])
        st.success(f"🥁 :red[Deal]  M70B Series 4K UHD USB-C Smart Monitor & Streaming TV,")

    with col_2:
        st.subheader(headers[1])
        st.image(product_images[1])
        st.success(f"🥁 :red[Deal] M80C UHD HDR Smart Computer Monitor Screen with Streaming TV")

    with col_3:
        st.subheader(headers[2])
        st.image(product_images[2])
        st.success(f"🥁 :red[Deal]       40-inch D-Series Full HD 1080p Smart TV")

# Assuming you have three distinct images for each column
image_names = ['indiana_jones', 'up', 'matrix']
column_headers = ['UHD TV', 'Ultra HD', 'Another cool TV']

# Construct the full paths for each image
script_directory = os.path.dirname(os.path.abspath(__file__))
full_image_paths = [os.path.join(script_directory, '..', 'images', 'products', 'movies', f"{image}.jpg") for image in image_names]

# Call the render_product_row function
render_product_row(full_image_paths, column_headers)

