FROM python:3.10-slim-bullseye
LABEL maintainer="@warias"
RUN apt-get update && apt-get install -y git
COPY . /app/
WORKDIR /app
EXPOSE 8501
RUN pip install -r requirements.txt 
ENTRYPOINT ["streamlit", "run"]
CMD ["app.py"]

